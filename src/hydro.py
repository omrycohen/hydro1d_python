import numba
import h5py as h5
from datetime import datetime
import numpy as np


geom = {"planar"      : (0, 1),
        "spherical"   : (2, 4 * np.pi), 
        "cylindrical" : (1, 2 * np.pi) }
        
alpha = 0
beta = 1


#@numba.jit
def calc_artificial_viscosity(geometry, cells_rho, partitions_vel, sigma = 1):
    diff_vel = np.diff(partitions_vel)
    indices = diff_vel < 0.
    artificial_viscosity = np.zeros(len(cells_rho))
    artificial_viscosity[indices] = (sigma * diff_vel ** 2 * cells_rho)[indices]
    return artificial_viscosity
     

#@numba.jit
def calc_volumes(geometry, partitions_pos):    
    return geometry[beta] / (geometry[alpha] + 1) * np.diff(partitions_pos ** (geometry[alpha] + 1))


#@numba.jit
def initialize_h5(name, number_of_cells):
    filename = name + " " + datetime.now().strftime("%d-%m-%Y %H-%M-%S") + ".hdf5"
    f = h5.File(filename, "w")
    f.attrs["name"] = name
    f.create_dataset("time",                 (0,),                     dtype='float64', maxshape = (None,))
    f.create_dataset("partitions_pos",       (0, number_of_cells + 1), dtype='float64', maxshape = (None, number_of_cells + 1))
    f.create_dataset("partitions_vel",       (0, number_of_cells + 1), dtype='float64', maxshape = (None, number_of_cells + 1))
    f.create_dataset("cells_pressure",       (0, number_of_cells),     dtype='float64', maxshape = (None, number_of_cells))
    f.create_dataset("cells_rho",            (0, number_of_cells),     dtype='float64', maxshape = (None, number_of_cells))    
    f.create_dataset("artificial_viscosity", (0, number_of_cells),     dtype='float64', maxshape = (None, number_of_cells))
    return f, filename


#@numba.jit
def write_to_h5(f, t, partitions_pos, partitions_vel, cells_pressure, 
                cells_rho, artificial_viscosity):

    vars = {"partitions_pos"       : partitions_pos,
            "partitions_vel"       : partitions_vel, 
            "cells_pressure"       : cells_pressure, 
            "cells_rho"            : cells_rho,
            "artificial_viscosity" : artificial_viscosity}

    ds = f["time"]
    ds.resize(tuple(map(sum, zip(ds.shape, (1,)))))
    ds[-1] = t

    for var in vars:
        ds = f[var]
        ds.resize(tuple(map(sum, zip(ds.shape, (1,0)))))
        ds[-1, :] = vars[var]


#@numba.jit
def dt_courant(partitions_pos, partitions_vel, cells_pressure, cells_rho, gamma):
    speed_of_sound = np.sqrt(gamma * cells_pressure / cells_rho)
    indices = (speed_of_sound != 0)
    if indices.any():
        return 0.25 * np.min(np.diff(partitions_pos)[indices] / (speed_of_sound[indices] \
                    + np.abs(partitions_vel[1:][indices]) + np.abs(partitions_vel[:-1][indices])))
    return np.inf


#@numba.jit
def dt_density(cells_volume_old, cells_volumes_new, dt_old, eps=1e-2):
    half_rel_change = np.abs((cells_volumes_new - cells_volume_old) / (cells_volumes_new + cells_volume_old))
    indices = (half_rel_change != 0)
    if indices.any():
        return 0.5 * dt_old * eps / np.max(half_rel_change[indices])
    return np.inf


#@numba.jit
def mainloop(name, geometry, t_max, 
             partitions_pos, partitions_vel, cells_pressure, 
             cells_rho, gamma):
    
    cells_volumes_old = cells_volumes = calc_volumes(geometry, partitions_pos)
    cells_mass = cells_rho * cells_volumes
    partitions_mass = np.convolve(cells_mass, np.array([0.5, 0.5]))
    artificial_viscosity = calc_artificial_viscosity(geometry, cells_mass / cells_volumes, partitions_vel)
    
    # Applying rigid boundary conditions implicitly
    partitions_acc = np.zeros(len(partitions_mass))
    partitions_acc[1:-1] = - geometry[beta] * partitions_pos[1:-1] ** geometry[alpha] * np.diff(cells_pressure + artificial_viscosity) / partitions_mass[1:-1]    
    
    t = 0
    dt = 1e-8 * t_max

    f, filename = initialize_h5(name, len(cells_pressure))
    write_to_h5(f, t, partitions_pos, partitions_vel, cells_pressure, 
                cells_rho, artificial_viscosity)

    percentage = last_percentage = 0
    print("0%")

    while t < t_max:

        # Print percentage      
        percentage = int(100 * t / t_max)
        if percentage > last_percentage:
            s = str(percentage) + "%"
            print(s)
            last_percentage = percentage

        # calc new dt using courant condition
        dt = min(1.1 * dt, 
                 dt_courant(partitions_pos, partitions_vel, cells_pressure, cells_mass / cells_volumes, gamma),
                 dt_density(cells_volumes_old, cells_volumes, dt))

        # stepping the partitions' positions        
        partitions_vel = partitions_vel + partitions_acc * dt / 2.
        partitions_pos = partitions_pos + partitions_vel * dt

        # calculating the new volumes and artificial viscosities
        cells_volumes_old = cells_volumes
        cells_volumes = calc_volumes(geometry, partitions_pos)
        artificial_viscosity_old = artificial_viscosity
        artificial_viscosity = calc_artificial_viscosity(geometry, cells_mass / cells_volumes, partitions_vel)

        # calculating new pressures
        aux = (cells_volumes - cells_volumes_old) / 2
        cells_pressure = (cells_volumes_old * cells_pressure / (gamma - 1) - aux * \
                         (cells_pressure + artificial_viscosity + artificial_viscosity_old)) / \
                         (cells_volumes / (gamma - 1) + aux)

        # calculating the next acceleration and velocities
        partitions_acc[1:-1] = - geometry[beta] * partitions_pos[1:-1] ** geometry[alpha] * np.diff(cells_pressure + artificial_viscosity) / partitions_mass[1:-1]    
        partitions_vel = partitions_vel + partitions_acc * dt / 2.

        # advancing time
        t += dt

        # saving data to external h5 file
        write_to_h5(f, t, partitions_pos, partitions_vel, cells_pressure, 
                    cells_mass / cells_volumes, artificial_viscosity)

    f.close()
    print("Done!")
    return filename
